<?php
require_once "functions.php";

$PageTitle = "Castor | Admin Page";
if (ft_is_admin())
{
    require_once "header.php";
    require_once "menu_admin.php";
    require_once "footer.php";
}
else
{
    echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
}
?>