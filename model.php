<?php

    function ft_connect_to_db()
    {
        $creds = json_decode(file_get_contents(".env"), true);
        $mysqli = mysqli_connect("localhost", $creds['user'], $creds['pass'], $creds['database']);
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        return ($mysqli);
    }
    
    function ft_execute_query($query)
    {
        mysqli_query(ft_connect_to_db(), $query);
    }
    
    function ft_get_query($query)
    {
        $res = mysqli_query(ft_connect_to_db(), $query);
        return (mysqli_fetch_assoc($res));
    }
    function ft_get_all_queries($query)
    {
        $res = mysqli_query(ft_connect_to_db(), $query);
        return ($res);
    }
?>