<?php

$PageTitle = "Castor | Login";

require_once "header.php";
require_once "functions.php";
require_once "menu.php";

session_start();

if ($_POST['login'] && $_POST['password'])
{
    $pass = hash("whirlpool", $_POST['password']);
    $res = ft_get_query("SELECT * FROM `users` WHERE login like '".$_POST['login']."';");
    if ($res)
    {
        if ($pass != $res['password'])
        {
            $err = "Parola  este incorecta";
        }
        else
        {
            $_SESSION['logged_in'] = "yes";
            $_SESSION['user_id'] = $res['id'];
            $_SESSION['start'] = time();
            $_SESSION['end'] = isset($_POST['remember']) ? time() + 31104000 : time() + 3600;
            ft_init_cart();
        }
    }
    else
        $err = "Login-ul este incorect";
}

if (!isset($_SESSION['logged_in']) && !isset($_SESSION['user_id']))
{
    ?>
    <form action="/login" method="POST">
        <div id="form">
            <table>
                <tr>
                    <td class="right"><label for="login">Login</label></td>
                    <td><input type="text" id="login" name="login" value="<?php echo $_POST['login'] ? $_POST['login'] : "";?>"/></td>
                </tr>
                <tr>
                    <td class="right"><label for="password">Password</label></td>
                    <td><input type="password" id="password" name="password"/></td>
                </tr>
                    <?php
                    if (isset($err))
                    {
                    ?>
                    <tr>
                    <td><p style="color:red;"><?php echo $err; ?></p></td>
                    </tr>
                    <?php
                    }
                    ?>
                <tr>
                    <td colspan="2"><input id="remember" type="checkbox" name='remember' value="2"/><label for="remember">Remember-me</label></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Login"/></td>
                </tr>
            </table>
        </div>
    </form>
    <?php
}
else
{
    header("Location: /");
}

?>