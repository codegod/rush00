<?php

$PageTitle = "Castor | Register";

require_once "header.php";
require_once "model.php";
require_once "menu.php";
session_start();

if ($_POST['login'] && $_POST['password'])
{
    $pass = hash("whirlpool", $_POST['password']);
    $res = ft_get_query("SELECT * FROM `users` WHERE login like '".$_POST['login']."';");
    if ($res)
        $err = "Acest login deja exista!";
    $res = ft_get_query("SELECT * FROM `users` WHERE email like '".$_POST['email']."';");
    if ($res)
        $err = "Acest email deja exista!";
        
    $ph = 1;
    
    if (!isset($err))
    {
        if (!empty($_FILES['image']['name']))
        {
            $imageFileType = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);  
            $target_dir = "/assets/images/" ."image_". hash("crc32", basename($_FILES["image"]["name"]) . time() . rand(1, 100)) . "." . $imageFileType;
            $target_file = $_SERVER['DOCUMENT_ROOT'] . $target_dir;
            $uploadOk = 1;

            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }

            if ($_FILES["image"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }

            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";

            } else {
                if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
            ft_execute_query("INSERT INTO `photos` (`id`, `address`) VALUES (NULL, '".$target_dir."');");
            $res = ft_get_query("SELECT * FROM  `photos` WHERE address LIKE  '".$target_dir."'");
            $ph = $res['id'];
        }
        
        ft_execute_query("INSERT INTO `users` (`id`, `surname`, `name`, `email`, `number`, `login`, `password`, `id_photo`, `sum`,
        `birth_date`, `signup_date`) VALUES (NULL, '".$_POST['surname']."', '".$_POST['name']."', '".$_POST['email']."', '".$_POST['number']."', '".$_POST['login']."',
        '".hash("whirlpool", $_POST['password'])."', '".$ph."', '0', '".$_POST['birth_date']."', '".date("Y-m-d")."');");
        
        $user = ft_get_query("SELECT * FROM  `users` WHERE login LIKE '".$_POST['login']."';");
        
        ft_execute_query("INSERT INTO `ctrler_priv` (`id_user`, `id_priv`) VALUES ('".$user['id']."', '2');");
        
        header("Location: /");
    }
}






if (!isset($_SESSION['logged_in']) && !isset($_SESSION['user_id']))
{
    ?>
    <form action="/register" method="POST" enctype="multipart/form-data">
        <div id="form">
                <table>

                    <tr>
                        <td class="right"><label for="name">Nume</label></td>
                        <td><input type="text" id="name" name="name" required/></td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="surname">Prenume</label></td>
                        <td><input type="text" id="surname" name="surname" required/></td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="email">Email</label></td>
                        <td><input type="email" id="email" name="email" required/></td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="number">Numar de telefon</label></td>
                        <td><input type="text" id="number" name="number" required/></td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="login">Login</label></td>
                        <td><input type="text" id="login" name="login" required/></td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="password">Parola</label></td>
                        <td><input type="password" id="password" name="password" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="birth_date">Data nasterii</label></td>
                        <td><input type="date" id="birth_date" name="birth_date" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="image">Imagine de profil</label></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="file" id="image" name="image"/></td>
                    </tr>
                    <?php
                    if (isset($err))
                    {
                    ?>
                    <tr>
                    <td><p color=red>Error: <?php echo $err; ?></p></td>
                    </tr>
                    <?php
                    }
                    ?>
                    <tr>
                        <td colspan="2"><input type="submit" value="Submit"/></td>
                    </tr>
                </table>

            </div>
        <!--<div id="form">-->
        <!--    <table>-->
        <!--        <tr>-->
        <!--            <td class="right"><label for="login">Login</label></td>-->
        <!--            <td><input type="text" id="login" name="login" value="<?php echo $_POST['login'] ? $_POST['login'] : "";?>"/></td>-->
        <!--        </tr>-->
        <!--        <tr>-->
        <!--            <td class="right"><label for="password">Password</label></td>-->
        <!--            <td><input type="password" id="password" name="password"/></td>-->
                    
        <!--        </tr>-->
        <!--        <tr>-->
        <!--            <td class="right"></td>-->
        <!--            <td><input id="remember" type="checkbox" name='remember' value="2"/><label for="remember">Remember-me</label></td>-->
        <!--        </tr>-->
        <!--        <tr>-->
        <!--            <td colspan="2"><input type="submit" value="Login"/></td>-->
        <!--        </tr>-->
        <!--    </table>-->
        <!--</div>-->
    </form>
    <?php
}
else
{
    header("Location: /");
}

?>