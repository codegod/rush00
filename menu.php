<?php

require_once "functions.php";

session_start();

ft_not_expired_session();

if (!file_exists(".env"))
        header("Location: /install");

  $user = ft_is_logged_in();


?>
 <nav id="primary_nav_wrap">
            <ul>
                <li><a style="font-family: 'Clicker Script', cursive; font-size:42px; float:left; clear:both;" href="/">Castor</a></li>
            </ul>
            <ul style="float:right;">
              <?php
              if (!isset($user))
              {
              ?>
                <li><a href="/login">Login</a></li>
                <li><a href="/register">Register</a></li>
                <?php
              }
              else
              {
                ?>
                <li><a href="/settings"><?php echo $user['name']. " ".$user['surname']; ?></a>
                <ul>
                  </li>
                  <?php if (ft_is_admin())
                  {
                  ?>
                  <li><a href="/admin">Admin Panel</a></li>
                  <?php
                  }
                  ?>
                  <li><a href="settings?action=add_funds">Wallet: <?php echo $user['sum']?></a></li>
                  <li><a href="/processor?show=cart">Cart: <?php $cart = ft_cart();  echo $cart['count'];?></a></li>
                  <li><a href="/logout">Logout</a></li>
                </ul>
                </li>
                <?php
              }
                ?>
            </ul><br><br>
<ul>
  <li ><a href="/">Home</a></li>
  
  <li><a href="/processor?show=guns">Guns</a>
    <ul>
      <li><a href="/processor?category=handgun">Handguns</a></li>
      <li><a href="/processor?category=shotgun">Shotguns</a></li>
      <li><a href="/processor?category=rifle">Rifles</a></li>
      <li><a href="/processor?category=muzzleloader">Muzzleloaders</a></li>
    </ul>
  </li>
  
  <li><a href="/information?show=contact">Contact Us</a></li>
  
  <li><a href="/information?show=about">About Us</a></li>
</ul>
</nav>