<?php
$PageTitle = "Castor | Settings";

require_once "header.php";
require_once "functions.php";
require_once "menu.php";
$user = ft_is_logged_in();

if ($_POST['card_num'] && $_POST['cvv'] && $_POST['amount'] && $_POST['submit'])
{
    $sum = $user['sum'] + $_POST['amount'];
    ft_execute_query("UPDATE  `users` SET  `sum` =  '".$sum."' WHERE  `users`.`id` =".$user['id'].";");
    header("Location: /");
}
elseif ($_GET['action'] == "add_funds")
{
?>
    <form action="settings" method="POST">
        <div id="form">
            <table>
                <tr>
                    <td class="right"><label for="card_num">Suma curenta</label></td>
                    <td><p><?php echo $user['sum']." $";?></p></td>
                </tr>
                <tr>
                    <td class="right"><label for="card_num">Card Number</label></td>
                    <td><input type="text" id="card_num" name="card_num" value=""/></td>
                </tr>
                <tr>
                    <td class="right"><label for="cvv">CVV</label></td>
                    <td><input type="text" id="cvv" name="cvv"/></td>
                </tr>
                <tr>
                    <td class="right"><label for="amount">Amount</td>
                    <td><input type="text" id="amount" name="amount"/></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" name="submit" value="OK"/></td>
                </tr>
            </table>
        </div>
    </form>
<?php
}
?>