<?php
$PageTitle = "Castor | Users";
require_once "header.php";
require_once "functions.php";
require_once "menu_admin.php";
session_start();
if (ft_is_admin())
{
    
    $users = ft_get_all_queries("SELECT * FROM `users`");
?>
<style type="text/css">
    table tr{
        text-align:left;
    }
</style>
    <div id="content">
        <table>
            <tr>
                <th>username</th>
                <th>name</th>
                <th>surname</th>
                <th>number</th>
                <th>email</th>
                <th>sum</th>
                <th>birth date</th>
                <th>signup date</th>
            </tr>
            <?php while ($user = mysqli_fetch_assoc($users)){ ?>
             <tr>
                <td><?php echo $user['login']; ?></td>
                <td><?php echo $user['name']; ?></td>
                <td><?php echo $user['surname']; ?></td>
                <td><?php echo $user['number']; ?></td>
                <td><?php echo $user['email']; ?></td>
                <td><?php echo $user['sum']; ?></td>
                <td><?php echo $user['birth_date']; ?></td>
                <td><?php echo $user['signup_date']; ?></td>
            </tr>
            <?php } ?>
        </table>
    </div>
<?php
}
else
{
    echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
}
?>