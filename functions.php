<?php

require_once "model.php";

function ft_import_tables($file)
{
    $templine = '';
    
    $lines = file($file);

    foreach ($lines as $line)
    {

        if (substr($line, 0, 2) == '--' || $line == '')
            continue;
        
        $templine .= $line;
    
        if (substr(trim($line), -1, 1) == ';')
        {
            ft_execute_query($templine);
    
            $templine = '';
        }
    }
    return (true);
}

function ft_is_logged_in()
{
    session_start();
    if (isset($_SESSION['logged_in']) && isset($_SESSION['user_id']))
        $user = ft_get_query("SELECT * FROM  `users` WHERE id =".$_SESSION['user_id']."");
    return ($user);
}

function ft_not_expired_session()
{
    session_start();
    if (isset($_SESSION['end']) && $_SESSION['end']  < time())
        header("Location: /logout");
}

function ft_is_admin()
{
    $user = ft_is_logged_in();
    if ($user)
    {
        $res = ft_get_query("SELECT * FROM `ctrler_priv` WHERE `id_user` = " . $user['id'] . " AND `id_priv` = 1;");
        if (count($res) > 1)
            return (true);
    }
    return (false);
}

function ft_init_cart()
{
    session_start();
    $data['count'] = '0';
    $data['products'] = array('none');
    $json = json_encode($data);
    $_SESSION['cart'] = $json;
}

function ft_cart()
{
    session_start();
    $json = $_SESSION['cart'];
    $arr = json_decode($json, true);
    return ($arr);
}

function ft_add_to_cart($data)
{
    session_start();
    $json = json_encode($data);
    $_SESSION['cart'] = $json;
}

function ft_get_products($category)
{
    if ($category == "all")
    {
        $sql = "SELECT * FROM `products` WHERE 1";
        $product_ids = ft_get_all_queries($sql);
        return ($product_ids ? $product_ids : false);
    }
    $categ = ft_get_query("SELECT * FROM `categories` WHERE slug like '".$category."'");

    $product_ids = ft_get_all_queries("SELECT * FROM `ctrler_cat` WHERE `id_category` = ".$categ['id'].";");

    if (!$product_ids)
        return (false);
    $row = mysqli_fetch_assoc($product_ids);
    $sql = "SELECT * FROM `products` WHERE `id` IN (".$row['id_product'];
    while ($row = mysqli_fetch_assoc($product_ids))
    {
        $sql .= ','.$row['id_product'];
    }
    $sql .= ");";
    
    $product_ids = ft_get_all_queries($sql);
    return ($product_ids);
}

function ft_get_prod_id($id)
{
    if ($id)
    {
        $prod = ft_get_query("SELECT * FROM `products` WHERE id = '".$id."'");
        return ($prod);
    }
    return (false);
}

?>