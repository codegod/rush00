<?php

    if (file_exists("./.env"))
        header("Location: /");

    require_once "model.php";
    require_once "functions.php";
    
    session_start();
    
    if (!isset($_SESSION['page']))
    {
        $_SESSION['page'] = 1;
    }
    
    if ($_POST['login_host'] && $_POST['pass_host'] && $_SESSION['page'] == 1)
    {
        $_SESSION['login_host'] = $_POST['login_host'];
        $_SESSION['pass_host'] = $_POST['pass_host'];
        $_SESSION['page'] = 2;
    }
    elseif ($_POST['database_name'] && $_SESSION['page'] == 2)
    {
        $_SESSION['database_name'] = $_POST['database_name'];
        $_SESSION['page'] = 3;
    }
    elseif ($_POST['login'] && $_POST['password'] && $_SESSION['page'] == 3)
    {
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['pass'] = $_POST['password'];
        $_SESSION['page'] = "Installed";
        
        $creds['user'] = $_SESSION['login_host'];
        $creds['pass'] = $_SESSION['pass_host'];
        $creds['database'] = $_SESSION['database_name'];
        file_put_contents(".env", json_encode($creds));
        
        ft_import_tables("web.sql");
        
        $ph = 1;
        
        if (!empty($_FILES['image']['name']))
        {
            $imageFileType = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);  
            $target_dir = "/assets/images/" ."image_". hash("crc32", basename($_FILES["image"]["name"]) . time() . rand(1, 100)) . "." . $imageFileType;
            $target_file = $_SERVER['DOCUMENT_ROOT'] . $target_dir;
            $uploadOk = 1;

            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }

            if ($_FILES["image"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }

            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";

            } else {
                if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
            ft_execute_query("INSERT INTO `photos` (`id`, `address`) VALUES (NULL, '".$target_dir."');");
            $res = ft_get_query("SELECT * FROM  `photos` WHERE address LIKE  '".$target_dir."'");
            $ph = $res['id'];
        }
        
        ft_execute_query("INSERT INTO `users` (`id`, `surname`, `name`, `email`, `number`, `login`, `password`, `id_photo`, `sum`,
        `birth_date`, `signup_date`) VALUES (NULL, '".$_POST['surname']."', '".$_POST['name']."', '".$_POST['email']."', '".$_POST['number']."', '".$_POST['login']."',
        '".hash("whirlpool", $_POST['password'])."', '".$ph."', '0', '".$_POST['birth_date']."', '".date("Y-m-d")."');");
        
        $user = ft_get_query("SELECT * FROM  `users` WHERE login LIKE '".$_POST['login']."';");
        
        ft_execute_query("INSERT INTO `ctrler_priv` (`id_user`, `id_priv`) VALUES ('".$user['id']."', '1');");
    
    }    
require_once "header.php";

?>

        <form action="install" method="POST" enctype="multipart/form-data">
            <div id="form">
                <?php
                if ($_SESSION['page'] != "Installed")
                    echo "<table>";
                if ($_SESSION['page'] == 1){
                ?>
                    <tr>
                        <td class="right"><label for="login_host">Login</label></td>
                        <td><input type="text" id="login_host" name="login_host" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="pass_host">Password</label></td>
                        <td><input type="password" id="pass_host" name="pass_host" required/></td>
                    </tr>
                <?php
                }
                elseif ($_SESSION['page'] == 2){
                ?>
                    <tr>
                        <td class="right"><label for="database_name">Numele bazei</label></td>
                        <td><input type="text" id="database_name" name="database_name" required/></td>
                    </tr>
                <?php
                }
                elseif ($_SESSION['page'] == 3){
                ?>
                    <tr>
                        <td class="right"><label for="name">Nume</label></td>
                        <td><input type="text" id="name" name="name" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="surname">Prenume</label></td>
                        <td><input type="text" id="surname" name="surname" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="email">Email</label></td>
                        <td><input type="email" id="email" name="email" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="number">Numar de telefon</label></td>
                        <td><input type="text" id="number" name="number" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="login">Login</label></td>
                        <td><input type="text" id="login" name="login" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="password">Parola</label></td>
                        <td><input type="password" id="password" name="password" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="birth_date">Data nasterii</label></td>
                        <td><input type="date" id="birth_date" name="birth_date" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="image">Imagine de profil</label></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="file" id="image" name="image"/></td>
                    </tr>
                <?php
                }
                elseif ($_SESSION['page'] == "Installed") {
                    echo "<p>Ai configurat cu succes!</p><br>";
                    header("Location: /");
                }
                if ($_SESSION['page'] != "Installed"){
                ?>
                    <tr>
                        <td colspan="2"><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
                <?php
                }
                ?>
            </div>
        </form>
<?php
require_once "footer.php";
?>