<?php

require_once "functions.php";
require_once "header.php";

if ($_GET['edit'])
    require_once "menu_admin.php";
else
    require_once "menu.php";

if ($_GET['show'])
{
    if ($_GET['show'] == 'guns')
        require 'content/all_guns.php';
    elseif ($_GET['show'] == 'cart')
        require 'content/order.php';
}
elseif ($_GET['category']) {
    $products = ft_get_products($_GET['category']);
    if ($products)
    {
        $action = "view";
        require "content/product_cat.php";
    }
    else {
    ?>   
        <div id="content">
            <p>Nothing to show!</p>
        </div>
    <?php
    }
}
elseif ($_GET['product']) {
    if ($_GET['product'] != '')
        require "content/single_page.php";
}
elseif ($_GET['edit'])
{
    if (ft_is_admin())
    {
        $products = ft_get_products($_GET['edit']);
        if ($products)
        {
            $action = "edit";
            require "content/product_cat.php";
        }
        else {
        ?>   
            <div id="content">
                <p>Nothing to show!</p>
            </div>
        <?php
        }
    }
    else
    {
        echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
    }
}
elseif ($_GET['edit_product'])
{
    if (ft_is_admin())
    {
        if ($_GET['edit_product'] != '')
            require "content/edit_page.php";
    }
    else
    {
        echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
    }
}
elseif ($_GET['cart']) {
    if (!ft_is_logged_in())
        header("Location: /login");
    $cart = ft_cart();
    if ($cart['count'] == 0)
    {
        $cart['products'][$cart['count']] = array('id' => $_GET['cart'], "count" => $_GET['qty']);
        $cart['count'] = 1;
        ft_add_to_cart($cart);
    }else{
        $was = 0;
        for ($i = 0; $i < $cart['count']; $i++) {
            if ($cart['products'][$i]['id'] == $_GET['cart'])
            {
                $was = 1;
                $cart['products'][$i]['count'] += $_GET['qty']; 
            }
        }
        if (!was)
        {
            $cart['products'][$cart['count'] + 1] = array('id' => $_GET['cart'], "count" => $_GET['qty']);
            $cart['count'] += 1;
        }
        ft_add_to_cart($cart);
    }
    header('Location: /processor?product='.$_GET['cart']);
}
require_once "footer.php";
?>