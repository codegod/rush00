<?php
$PageTitle = "Castor | Add_gun";
require_once "header.php";
require_once "functions.php";
require_once "menu_admin.php";
session_start();

if (ft_is_admin())
{
    
    if ($_POST['gun_name'] && $_POST['type'] && $_POST['price'] && $_POST['stock'] && $_POST['submit'])
    {
         foreach($_FILES["image"]["error"] as $key => $value)
        {
            if ($value == UPLOAD_ERR_OK)
            {
                $imageFileType = pathinfo($_FILES['image']['name'][$key], PATHINFO_EXTENSION); 
                $uploadOk = 1;
    
                if (file_exists($target_file)) {
                    die("Sorry, file already exists.");
                    $uploadOk = 0;
                }
    
                if ($_FILES["image"]["size"][$key] > 500000) {
                    die("Sorry, your file is too large.");
                    $uploadOk = 0;
                }
    
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                    var_dump($imageFileType);
                    die("Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
                    $uploadOk = 0;
                }
    
                if ($uploadOk == 0) {
                    die("Sorry, your file was not uploaded.");
    
                }
            }
        }
                
        ft_execute_query("INSERT INTO `products`(`name`, `price`, `add_date`, `info`) VALUES ('".$_POST['gun_name']."',".$_POST['price'].",'".date("Y-m-d")."','".$_POST['info']."');");
        $prods = ft_get_all_queries("SELECT * FROM `products`");
        while ($row = mysqli_fetch_assoc($prods))
            $temp = $row;
        ft_execute_query("INSERT INTO `ctrler_cat`(`id_product`, `id_category`) VALUES (".$temp['id'].",".$_POST['type'].")");
        ft_execute_query("INSERT INTO `stock`(`id_product`, `count`) VALUES (".$temp['id'].", ".$_POST['stock'].")");
        foreach($_FILES["image"]["error"] as $key => $value)
        {
            if ($value == UPLOAD_ERR_OK)
            {
    
                
                $imageFileType = pathinfo($_FILES['image']['name'][$key], PATHINFO_EXTENSION);  
                
                $target_dir = "/assets/images/" ."image_". hash("crc32", basename($_FILES["image"]["name"][$key]) . time() . rand(1, 100)) . "." . $imageFileType;
                $target_file = $_SERVER['DOCUMENT_ROOT'] . $target_dir;
                $uploadOk = 1;
    
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }
    
                if ($_FILES["image"]["size"][$key] > 500000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }
    
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOk = 0;
                }
    
                if ($uploadOk == 0) {
                    echo "Sorry, your file was not uploaded.";
    
                } else {
                    if (!move_uploaded_file($_FILES["image"]["tmp_name"][$key], $target_file)) {
                        echo "Sorry, there was an error uploading your file.";
                    }else{
                        ft_execute_query("INSERT INTO `photos` (`id`, `address`) VALUES (NULL, '".$target_dir."');");
                        $res = ft_get_query("SELECT * FROM  `photos` WHERE address LIKE  '".$target_dir."'");
                        $ph = $res['id'];
                        ft_execute_query("INSERT INTO `ctrler_photo`(`id_photo`, `id_product`) VALUES (".$ph.",".$temp['id'].");");
                        
                    }
                }
                
                
            }
            else{
                $error++;
            }
        }
        if ($_FILES['cover_image']['size'] == 0 && $_FILES['cover_image']['error'] == 0){
            ft_execute_query("INSERT INTO `ctrler_photo`(`id_photo`, `id_product`) VALUES (1,".$temp['id'].");");
        }
    }
    else
    {
    
    
    
    
    ?>
    <form action="/add_gun" method="POST" enctype="multipart/form-data">
        <div id="form">
           <table>

                    <tr>
                        <td class="right"><label for="gun_name">Name</label></td>
                        <td><input type="text" id="gun_name" name="gun_name" required/></td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="type">Type</label></td>
                        <td>
                            <select name="type">
                            <option value="1">handgun</option>
                            <option value="2">shotgun</option>
                            <option value="3">rifle</option>
                            <option value="4">muzzleloader</option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="price">Price</label></td>
                        <td><input type="text" id="price" name="price" required/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="stock">Stock</label></td>
                        <td><input type="text" id="stock" name="stock" required/></td>
                    </tr>
                
                <tr>
                        <td class="right"><label for="info">Info</label></td>
                        <td><textarea id="info" name="info"></textarea></td>
                    </tr>
                
                    <tr>
                        <td class="right"><label for="gun_image">Image</label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            
                            <input type="file" id="gun_image" accept="image/*" multiple name="image[]" />
                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="submit" name="submit" value="Submit"/></td>
                    </tr>
                </table>
        </div>
    </form>
    <?php
    }
}
else
{
    echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
}

?>