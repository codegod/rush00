<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $PageTitle ? $PageTitle : "Castor"; ?></title>
        <link rel="stylesheet" type="text/css" href="assets/css/style.css"> 
        <link rel="stylesheet" href="assets/css/menu.css" type="text/css" /> 
        <link href="https://fonts.googleapis.com/css?family=Clicker+Script" rel="stylesheet">
        <link rel="icon" type="image/png" href="assets/images/favicon.png">
    </head>
    <body>