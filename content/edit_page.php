<?php
require_once "functions.php";
if (ft_is_admin())
{
    
    
    if ($_POST['gun_name'] && $_POST['type'] && $_POST['price'] && $_POST['stock'] && $_POST['submit'])
    {
        if ($_POST['submit'] == "Update")
        {
            foreach($_FILES["image"]["error"] as $key => $value)
            {
                if ($value == UPLOAD_ERR_OK)
                {
                    $imageFileType = pathinfo($_FILES['image']['name'][$key], PATHINFO_EXTENSION); 
                    $uploadOk = 1;
        
                    if (file_exists($target_file)) {
                        die("Sorry, file already exists.");
                        $uploadOk = 0;
                    }
        
                    if ($_FILES["image"]["size"][$key] > 500000) {
                        die("Sorry, your file is too large.");
                        $uploadOk = 0;
                    }
        
                    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                        var_dump($imageFileType);
                        die("Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
                        $uploadOk = 0;
                    }
        
                    if ($uploadOk == 0) {
                        die("Sorry, your file was not uploaded.");
        
                    }
                }
            }
            
            $sql = "UPDATE `products` SET  `name` =  '".$_POST['gun_name']."', `price` =  '".$_POST['price']."', `add_date` =  '".date("Y-m-d")."', `info` =  '".$_POST['info']."'  WHERE  `products`.`id` =".$_GET['edit_product'].";";
    
            ft_execute_query($sql);
    
            
            $temp = $_GET['edit_product'];
    
            ft_execute_query("UPDATE `ctrler_cat` SET `id_category`='".$_POST['type']."' WHERE `id`=".$temp.";");
            ft_execute_query("UPDATE `stock` SET `count`='".$_POST['stock']."' WHERE `id_product`=".$temp.";");        
            foreach($_FILES["image"]["error"] as $key => $value)
            {
                if ($value == UPLOAD_ERR_OK)
                {
        
                    
                    $imageFileType = pathinfo($_FILES['image']['name'][$key], PATHINFO_EXTENSION);  
                    
                    $target_dir = "/assets/images/" ."image_". hash("crc32", basename($_FILES["image"]["name"][$key]) . time() . rand(1, 100)) . "." . $imageFileType;
                    $target_file = $_SERVER['DOCUMENT_ROOT'] . $target_dir;
                    $uploadOk = 1;
        
                    if (file_exists($target_file)) {
                        echo "Sorry, file already exists.";
                        $uploadOk = 0;
                    }
        
                    if ($_FILES["image"]["size"][$key] > 500000) {
                        echo "Sorry, your file is too large.";
                        $uploadOk = 0;
                    }
        
                    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOk = 0;
                    }
        
                    if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
        
                    } else {
                        if (!move_uploaded_file($_FILES["image"]["tmp_name"][$key], $target_file)) {
                            echo "Sorry, there was an error uploading your file.";
                        }else{
                            ft_execute_query("INSERT INTO `photos` (`id`, `address`) VALUES (NULL, '".$target_dir."');");
                            $res = ft_get_query("SELECT * FROM  `photos` WHERE address LIKE  '".$target_dir."'");
                            $ph = $res['id'];
                            ft_execute_query("INSERT INTO `ctrler_photo`(`id_photo`, `id_product`) VALUES (".$ph.",".$temp.");");
                        }
                    }
                    
                    
                }
                else{
                    $error++;
                }
                
            }
        }elseif ($_POST['submit'] == "Delete") {
            ft_execute_query("DELETE FROM `ctrler_cat` WHERE `id_product` = ".$_GET['edit_product'].";");
            $photos = ft_get_all_queries("SELECT * FROM `ctrler_photo` WHERE `id_product` = ".$_GET['edit_product'].";");
            while ($row = mysqli_fetch_assoc($photos))
            {
                $ph = ft_get_query("SELECT * FROM `photos` WHERE `id`=".$row['id_photo'].";");
                if ($row['id_photo'] != 1){
                    unlink($_SERVER['DOCUMENT_ROOT'].$ph['address']);
                    ft_execute_query("DELETE FROM `photos` WHERE `id`=".$row['id_photo'].";");
                }
            }
            ft_execute_query("DELETE FROM `ctrler_photo` WHERE `id_product`=".$_GET['edit_product'].";");
            ft_execute_query("DELETE FROM `stock` WHERE `id_product`=".$_GET['edit_product'].";");
            ft_execute_query("DELETE FROM `products` WHERE `id`=".$_GET['edit_product'].";");
  
        }
    }
    
    
    
    if ($_POST['submit'] != "Delete")
    {
        
    
    
        $prod = ft_get_prod_id($_GET['edit_product']);
        
        $photo_all = ft_get_all_queries("SELECT * FROM `ctrler_photo` WHERE `id_product` = ".$_GET['edit_product'].";");
        $photo_id = mysqli_fetch_assoc($photo_all);
        $photo = ft_get_query("SELECT * FROM `photos` WHERE `id` = ".$photo_id['id_photo'].";");
        $stock = ft_get_query("SELECT * FROM `stock` WHERE `id_product` = ".$prod['id'].";");
    }
    
?>
    
    <form action="/processor?edit_product=<?php echo $_GET['edit_product']; ?>" method="POST" enctype="multipart/form-data">
        <div id="form">
           <table>

                    <tr>
                        <td class="right"><label for="gun_name">Name</label></td>
                        <td><input type="text" id="gun_name" name="gun_name" required value="<?php echo $prod['name']; ?>"/></td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="type">Type</label></td>
                        <td>
                            <select name="type">
                            <option value="1">handgun</option>
                            <option value="2">shotgun</option>
                            <option value="3">rifle</option>
                            <option value="4">muzzleloader</option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="right"><label for="price">Price</label></td>
                        <td><input type="text" id="price" name="price" required value="<?php echo $prod['price']; ?>"/></td>
                    </tr>
                    <tr>
                        <td class="right"><label for="stock">Stock</label></td>
                        <td><input type="text" id="stock" name="stock" required value="<?php echo $stock['count']; ?>"/></td>
                    </tr>
                
                <tr>
                        <td class="right"><label for="info">Info</label></td>
                        <td><textarea id="info" name="info"><?php echo $prod['info']; ?></textarea></td>
                    </tr>
                
                    <tr>
                        <td class="right"><label for="gun_image">Image</label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            
                            <input type="file" id="gun_image" accept="image/*" multiple name="image[]" />
                        
                        </td>
                    </tr>
                    <tr>
                        <td ><input type="submit" name="submit" value="Update"/></td>
                        <td ><input type="submit" name="submit" value="Delete"/></td>
                    </tr>
                </table>
        </div>
    </form>
    
<?php
}else
    echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";

?>