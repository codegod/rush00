<?php

if (ft_is_logged_in())
{
    
   $var = ft_cart();
   $arr = $var['products'];
   $sum = 0;
?>
<style type="text/css">
    table tr{
        text-align:left;
    }
</style>
    <div id="content">
        <table>
            <tr>
                <th>Product name</th>
                <th>Price</th>
                <th>Amount</th>
            </tr>
            <?php foreach ($arr as $key => $val) {
                $prod = ft_get_prod_id($val['id']);
                $sum = $sum + $prod['price'] * $val['count'];
                ?>
             <tr>
             <td><?php echo $prod['name']; ?></td>
              <td><?php echo $prod['price']; ?></td>
               <td><?php echo $val['count']; ?></td>
            </tr>
          <?php } ?>
          <tr>
              <td>Summ: <?php echo $sum; ?></td>
          </tr>
        </table>
    </div>
<?php
}else
{
    echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
}
?>